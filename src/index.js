import express from 'express';
import cors from 'cors';

import canonize from './canonize';

const app = express();

const corsOptions = {
  origin: 'http://account.skill-branch.ru',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

app.get('/task2C', (req, res) => {
	let username = canonize(req.query.username);
	if(!username){
		res.send('Invalid username');
	}else{
		res.send('@' + username);
	}
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
